package fragments;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.tradiitions.R;
import com.android.tradiitions.activities.EventDetail;
import com.android.tradiitions.activities.LoginActivity;
import com.android.tradiitions.objects.Event;
import com.android.tradiitions.objects.User;
import com.android.tradiitions.util.EventArrayAdapter;
import com.android.tradiitions.util.SessionManager;

/* Home page
 * Author: Alex Yu
 * Date:1/31/2014
 * Displays user info and event list
 */

/*TODO: Implement rest call to retrieve achievement details from server*/

public class HomePageFrag extends Fragment{
	SessionManager session;
	ArrayList<Event> elist;
	Bundle bundle;
	User user;
	ProgressBar progressBar;
	ImageView profile;
	
    @Override  
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {  
        View rootView = inflater.inflate(R.layout.fragment_user_detail, container, false);  
        
        bundle = this.getArguments();
        elist = bundle.getParcelableArrayList(LoginActivity.EVENTDATA);
        user = bundle.getParcelable(LoginActivity.USERDATA);
		ListView lv = (ListView) rootView.findViewById(android.R.id.list);

		final EventArrayAdapter adapter = new EventArrayAdapter(this.getActivity(),
				android.R.layout.simple_list_item_1, elist);

		lv.setAdapter(adapter);

		lv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, final View view,
					int position, long id) {
				Intent i = new Intent(getActivity(), EventDetail.class);
				i.putExtra("FOREVENTDETAIL", elist.get(position));
				startActivity(i);
			}
		});
		
		setUserFields(rootView);
		
        return rootView;  
    }  
    
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        session = new SessionManager(activity);
    }
    
    public void setUserFields(View rView) {
		TextView userView = (TextView) rView.findViewById(R.id.nameView);
		String fullName = user.getFName() + " " + user.getLName();
		userView.setText(fullName);
		
		profile = (ImageView) rView.findViewById(R.id.imageView1);
		
		int score = user.getScore();
		if (score < 500) {
			profile.setImageDrawable(getResources().getDrawable(R.drawable.home_hawk1));
		}
		else if (score < 1000) {
			profile.setImageDrawable(getResources().getDrawable(R.drawable.home_hawk2));
		}
		else if (score < 1500) {
			profile.setImageDrawable(getResources().getDrawable(R.drawable.home_hawk3));
		}
		else if (score < 2000) {
			profile.setImageDrawable(getResources().getDrawable(R.drawable.home_hawk4));			
		}
		else {
			profile.setImageDrawable(getResources().getDrawable(R.drawable.home_hawk5));
		}
		progressBar = (ProgressBar) rView.findViewById(R.id.progressBar1);
		progressBar.setProgress(user.getScore());
		progressBar.setMax(500);		
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (profile != null) {
            // Cancel any pending image work
            profile.setImageDrawable(null);
        }
    }
}