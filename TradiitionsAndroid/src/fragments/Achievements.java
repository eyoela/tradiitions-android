package fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.tradiitions.R;

/*Achievements detail page 
 * Author: Alex Yu
 * Date:1/31/2014
 * Displays a list and images of all achievements 
 */

/*TODO: Implement rest call to retrieve achievement details from server*/

public class Achievements extends Fragment{
    @Override  
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {  
        View rootView = inflater.inflate(R.layout.fragment_achievement, container, false);  
        TextView dummyTextView = (TextView) rootView.findViewById(R.id.section_label);  
        dummyTextView.setText("Achieveos!");  
        return rootView;  
    }
    
    
}
