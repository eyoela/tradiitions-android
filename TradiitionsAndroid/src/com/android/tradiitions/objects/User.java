package com.android.tradiitions.objects;

import java.util.Date;
import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

/* User POJO
 * Author: Alex Yu
 * Date:1/31/2014
 * Object representing a user's JSON data
 * */

public class User implements Parcelable  {
	public Date created_at;
	
	public int level;
	public int score;

	public String fname;
	public String lname;
	public String cwid;
	public String email;
	
	public List<Event> attendedEvents;


	public User(Date created_at, int level, int score, String f_name,
			String l_name, String cwid, String email) {
		this.created_at = created_at;
		this.level = level;
		this.score = score;
		this.fname = f_name;
		this.lname = l_name;
		this.cwid = cwid;
		this.email = email;
	}
	
    public User(Parcel in) {  
        readFromParcel(in);  
    }  

	public User() {
		this.created_at = new Date();
		this.level = 0;
		this.score = 0;
		this.fname = " ";
		this.lname = " ";
		this.cwid = " ";
		this.email = " ";	
	}

	public String getFName() {
		return fname;
	}
	
	public void setFName(String f_name) {
		this.fname = f_name;
	}
	
	public String getLName() {
		return lname;
	}
	
	public void setLName(String l_name) {
		this.lname = l_name;
	}
	
	public String getCwid() {
		return cwid;
	}
	
	public void setCwid(String cwid) {
		this.cwid = cwid;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public Date getCreated_at() {
		return created_at;
	}
	
	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}
	
	public int getLevel() {
		return level;
	}
	
	public void setLevel(int level) {
		this.level = level;
	}
	
	public int getScore() {
		return score;
	}
	
	public void setScore(int score) {
		this.score = score;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int arg1) {
		// TODO Auto-generated method stub
		out.writeString(fname);
		out.writeString(lname);
		out.writeString(cwid);
		out.writeString(email);
		
		out.writeInt(level);
		out.writeInt(score);
		
		out.writeLong(created_at.getTime());
	}	
	
    private void readFromParcel(Parcel in) {    
		fname = in.readString();
		lname = in.readString();
		cwid = in.readString();
		email = in.readString();
		
		level = in.readInt();
		score = in.readInt();
		
		created_at = new Date(in.readLong());
    } 
    
    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {  
        
        public User createFromParcel(Parcel in) {  
            return new User(in);  
        }  
   
        public User[] newArray(int size) {  
            return new User[size];  
        }  
          
    }; 
    
    public String toString() {
    	return fname + " " + lname + " " + cwid;
    }
}
