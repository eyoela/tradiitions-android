package com.android.tradiitions.objects;

import java.util.Date;

import android.os.Parcel;
import android.os.Parcelable;

/* Event POJO
 * Author: Alex Yu
 * Date:1/31/2014
 * Object representing an event's JSON data
 * */

public class Event implements Parcelable {
	public String time;
	
	public int id;
	public int event_score;
	
	public String title;
	public String imageURL;
	public String eventURL;
	public String description;
	public String location;
	
	public Event(String time, int id, String title) {
		this.time = time;
		this.id = id;
		this.title = title;
	}
	
	public Event(Parcel in) {
        readFromParcel(in);  
	}

	public int getEvent_score() {
		return event_score;
	}
	
	public void setEvent_score(int event_score) {
		this.event_score = event_score;
	}
	
	public String getImageURL() {
		return imageURL;
	}
	
	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}
	
	public String getEventURL() {
		return eventURL;
	}
	
	public void setEventURL(String eventURL) {
		this.eventURL = eventURL;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getLocation() {
		return location;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}
		
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		// TODO Auto-generated method stub
		out.writeString(title);
		out.writeString(imageURL);
		out.writeString(eventURL);
		out.writeString(description);
		out.writeString(location);
		out.writeInt(id);
		out.writeInt(event_score);
		out.writeString(time);
	}
	
    private void readFromParcel(Parcel in) {    
		title = in.readString();
		imageURL = in.readString();
		eventURL = in.readString();
		description = in.readString();
		location = in.readString();

		id = in.readInt();
		event_score = in.readInt();
		
		time = in.readString();
    } 
    
    public static final Parcelable.Creator<Event> CREATOR = new Parcelable.Creator<Event>() {  
        
        public Event createFromParcel(Parcel in) {  
            return new Event(in);  
        }  
   
        public Event[] newArray(int size) {  
            return new Event[size];  
        }  
          
    }; 
    
    @Override
	public String toString() {
		return "Event [time=" + time.toString() + ", id=" + id + ", event_score="
				+ event_score + ", title=" + title + ", imageURL=" + imageURL
				+ ", eventURL=" + eventURL + ", description=" + description
				+ ", location=" + location + "]";
	}
}
