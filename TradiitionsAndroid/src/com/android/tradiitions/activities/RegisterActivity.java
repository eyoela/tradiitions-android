package com.android.tradiitions.activities;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.tradiitions.R;

/* Register Page
 * Author: Alex Yu
 * Date:1/31/2014
 * Register the user
 * Navigates to Login Page
 * */

/* TODO: implement validation code via email when registering, 
 * update graphics, research performance enhancements*/

public class RegisterActivity extends Activity {
	private final String GETURL = "http://techtradiitions.com:8080/";

	String firstName, lastName, campusID, email;
	EditText fnameField, lnameField, emailField, cwidField; 
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);
		
		fnameField = (EditText) findViewById(R.id.edit_fname);
		lnameField = (EditText) findViewById(R.id.edit_lname);
		emailField = (EditText) findViewById(R.id.edit_email);
		cwidField = (EditText) findViewById(R.id.edit_anum);
	}
	
	public void registerUser(View view) {
		firstName = fnameField.getText().toString();
		lastName = lnameField.getText().toString();
		campusID = cwidField.getText().toString();
		email = emailField.getText().toString();
		firstName = firstName.replaceAll(" ", "_");
		
		if(firstName.trim().length() > 0 && lastName.trim().length() > 0 && email.trim().length() > 0 && campusID.trim().length() > 0) {                 
			// Search for user asynchronously (even though we make him or her wait anyway huehuehue)
			Toast.makeText(getApplicationContext(), "Adding user...", Toast.LENGTH_SHORT).show();
			new RegisterUserTask().execute();
		} 
	}
	
	public void finishReg(View view) {
		finish();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.register, menu);
		return true;
	}

	private class RegisterUserTask extends AsyncTask <Void, Void, String> {
		@Override
		protected String doInBackground(Void... params) {
			HttpClient httpClient = new DefaultHttpClient();
			HttpContext localContext = new BasicHttpContext();
			String getURL = GETURL + "addUser/" + campusID+"/"+firstName+"/"+lastName+"/"+email;
			HttpGet httpGet = new HttpGet(getURL);
			String text = null;
			try {
				HttpResponse response = httpClient.execute(httpGet, localContext);
				HttpEntity entity = response.getEntity();
				BufferedReader reader = new BufferedReader(new InputStreamReader(entity.getContent(), "UTF-8"));				
				text=reader.readLine();
			} catch (Exception e) {
				return e.getLocalizedMessage();
			}
			return text;
		}

		protected void onPostExecute(String results) {
			if(results != null) {
				int statusCode;
				try {
					JSONObject addResponse = new JSONObject(results);
					statusCode = addResponse.getInt("status");
					if(statusCode == 0) {
						Toast.makeText(getApplicationContext(), addResponse.getString("message"), Toast.LENGTH_LONG).show();
					}
					else
						Toast.makeText(getApplicationContext(), "Succesfully registered!", Toast.LENGTH_LONG).show();
				} catch (JSONException e) {
					e.printStackTrace();
				}
			} 
			else {
				Toast.makeText(getApplicationContext(), "User not found, please re-enter Student ID.", Toast.LENGTH_LONG).show();
			}
		}
	}
}
