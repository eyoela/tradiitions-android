/* Home page
 * Author: Alex Yu
 * Date:1/31/2014
 * Displays the user's details and a list of current events.
 * Navigates to all other activities
 * */
package com.android.tradiitions.activities;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import com.android.tradiitions.R;
import com.android.tradiitions.objects.User;
import com.android.tradiitions.util.SessionManager;
import fragments.Achievements;
import fragments.HomePageFrag;
import fragments.Yearbook;

/* TODO: improve activity life cycle management, add navigation to other
 * activities in action bar*/

public class HomePageActivity extends FragmentActivity implements
ActionBar.TabListener {
	SessionManager session;
	Intent intent;
	@SuppressLint("NewApi")
	
	User user = null;
	
	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a
	 * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
	 * will keep every loaded fragment in memory. If this becomes too memory
	 * intensive, it may be best to switch to a
	 * {@link android.support.v4.app.FragmentStatePagerAdapter}.
	 */
	SectionsPagerAdapter mSectionsPagerAdapter;

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	ViewPager mViewPager;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home_page);
	
		
		session = new SessionManager(getApplicationContext());
		intent = getIntent();

		session.checkLogin();
				
		Bundle bundle = new Bundle();
		
		bundle.putParcelable(LoginActivity.USERDATA, intent.getParcelableExtra(LoginActivity.USERDATA));
		bundle.putParcelableArrayList(LoginActivity.EVENTDATA, intent.getParcelableArrayListExtra(LoginActivity.EVENTDATA));

		// Set up the action bar.
		final ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setDisplayHomeAsUpEnabled(false);
		// Create the adapter that will return a fragment for each of the three
		// primary sections of the app.
		mSectionsPagerAdapter = new SectionsPagerAdapter(
				getSupportFragmentManager(), bundle);

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);

		// When swiping between different sections, select the corresponding
		// tab. We can also use ActionBar.Tab#select() to do this if we have
		// a reference to the Tab.
		mViewPager
				.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
					@Override
					public void onPageSelected(int position) {
						actionBar.setSelectedNavigationItem(position);
					}
				});

		// For each of the sections in the app, add a tab to the action bar.
		for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
			// Create a tab with text corresponding to the page title defined by
			// the adapter. Also specify this Activity object, which implements
			// the TabListener interface, as the callback (listener) for when
			// this tab is selected.
			actionBar.addTab(actionBar.newTab()
					.setText(mSectionsPagerAdapter.getPageTitle(i))
					.setTabListener(this));
		}
	}

	public void onStart(Bundle savedInstanceState) {
		super.onStart();
	}

	public void logout() {
		session.logoutUser();
	}

	@Override
	public void onTabSelected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
		// When the given tab is selected, switch to the corresponding page in
		// the ViewPager.
		if (tab.getPosition() == 0) {
			mViewPager.setCurrentItem(tab.getPosition());
		}
		else 
			mViewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

	@Override
	public void onTabReselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {
		private Bundle bundle;
		
		public SectionsPagerAdapter(FragmentManager fm, Bundle bund) {
			super(fm);
			bundle = bund;
		}

		@Override
		public Fragment getItem(int position) {
		    Fragment fragment = new Fragment();  
		    switch (position) {  
		    case 0:
		    	fragment = new HomePageFrag();
		    	fragment.setArguments(bundle);
		    	return fragment;
		    case 1:  
		        return fragment = new Achievements(); 
		    case 2:
		    	return fragment = new Yearbook();
		    default:  
		        break;  
		    }  
		    return fragment; 
		}

		@Override
		public int getCount() {
			// Show 3 total pages.
			return 3;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			switch (position) {
			case 0:
				return "Home";
			case 1:
				return "Achievements";
			case 2:
				return "Yearbook";
			}
			return null;
		}
	}
}
