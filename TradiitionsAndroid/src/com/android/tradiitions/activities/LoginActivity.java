package com.android.tradiitions.activities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.tradiitions.R;
import com.android.tradiitions.objects.Event;
import com.android.tradiitions.objects.User;
import com.android.tradiitions.util.SessionManager;

/* Login Page
 * Author: Alex Yu
 * Date:1/31/2014
 * Validates the user's details and retrieves user data and event data for application
 * Navigates to Home Page
 * */

/* TODO: implement more response status code checking, update graphics, research performance enhancements*/

public class LoginActivity extends Activity {

	public final static String USERNAME = "com.android.tradiitions.USERNAME";
	public final static String CWID = "com.android.tradiitions.CWID";
	public final static String USERDATA = "com.android.tradiitions.USERDATA";
	public final static String EVENTDATA = "com.android.tradiitions.EVENTDATA";

	private final String GETURL = "http://techtradiitions.com:8080/";
	String username, campusID;
	boolean success;

	EditText userField, cwidField; 
	SessionManager session;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		session = new SessionManager(getApplicationContext());   

		userField = (EditText) findViewById(R.id.edit_user);
		cwidField = (EditText) findViewById(R.id.edit_cwid);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.login_activity, menu);
		return true;
	}

	public void checkUser(View view) throws ClientProtocolException, IOException {
		username = userField.getText().toString();
		campusID = cwidField.getText().toString();
		// Check if username, password is filled                
		if(username.trim().length() > 0 && campusID.trim().length() > 0) {                 
			// Search for user asynchronously (even though we make him or her wait anyway huehuehue)
			Toast.makeText(getApplicationContext(), "Searching for user...", Toast.LENGTH_SHORT).show();
			new LongRunningGetIO().execute();    			 
		} 
		else {
			// User didn't enter username or password
			Toast.makeText(getApplicationContext(), "Please enter username and password", Toast.LENGTH_LONG).show();
		}
	}
	
	public void regUser(View view) {
		startActivity(new Intent(getApplicationContext(), RegisterActivity.class));
	}
	
	private void logUser(String[] resultData) {
		if (resultData != null) {
			ArrayList<Event> elist = new ArrayList<Event>();
			User user = new User();
			try {
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
	                    java.util.Locale.getDefault());
				// Parse and verify user object
				JSONObject json = new JSONObject(resultData[0]);
				int statusCode = json.getInt("status");
				if (statusCode != 1) {
					Toast.makeText(getApplicationContext(), "User not found, please re-enter Student ID.", Toast.LENGTH_LONG).show();
					return;
				}
				JSONObject justUserObj = json.getJSONObject("message").getJSONObject("objects");

				Date parsedDate = formatter.parse(justUserObj.getString("created_at"));

				user = new User(parsedDate, justUserObj.getInt("level"), justUserObj.getInt("score"), justUserObj.getString("f_name"), 
						justUserObj.getString("l_name"), justUserObj.getString("cwid"), justUserObj.getString("email"));
				JSONObject eventJson = new JSONObject(resultData[1]);
				JSONArray events = eventJson.getJSONObject("message").getJSONArray("objects");
				elist = new ArrayList<Event>();
				if (events != null) { 
					   int len = events.length();
					   for (int i=0;i<len;i++){ 
						   JSONObject object = events.getJSONObject(i);	
						   Date date = new Date(object.getLong("time")*1000L); // *1000 is to convert minutes to milliseconds
						   java.text.DateFormat format = DateFormat.getDateFormat(getApplicationContext());
						   String dateString = format.format(date);
						   format = DateFormat.getTimeFormat(getApplicationContext());
						   format.setTimeZone(TimeZone.getDefault());
						   String timeString = format.format(date);
						   elist.add(new Event(dateString+" "+timeString, object.getInt("id"), object.getString("title")));   
					   } 
					} 
			} catch (Exception e) {
				
			}
			session.createLoginSession(username, user.getCwid(), user.getFName()+" "+user.getLName());
			
			// Start DisplayMessageActivity
			Intent i = new Intent(getApplicationContext(), HomePageActivity.class);
			i.putExtra(USERDATA, user);
			i.putParcelableArrayListExtra(EVENTDATA, elist);
			startActivity(i);
		} 
	}
	
	private class LongRunningGetIO extends AsyncTask <Void, Void, String[]> {
		@Override
		protected String[] doInBackground(Void... params) {
			HttpClient httpClient = new DefaultHttpClient();
			HttpContext localContext = new BasicHttpContext();
			String getUserURL = GETURL + "users/"+ campusID;
			HttpGet httpGetUser = new HttpGet(getUserURL);
			
			String getEventURL = GETURL + "events/";
			HttpGet httpGetEvent = new HttpGet(getEventURL);

			String[] text = new String[2];
			try {			
				HttpResponse response = httpClient.execute(httpGetUser, localContext);
				HttpEntity entity = response.getEntity();
				BufferedReader reader = new BufferedReader(new InputStreamReader(entity.getContent(), "UTF-8"));				
				text[0]=reader.readLine();

				HttpResponse response2 = httpClient.execute(httpGetEvent, localContext);
				HttpEntity entity2 = response2.getEntity();
				BufferedReader reader2 = new BufferedReader(new InputStreamReader(entity2.getContent(), "UTF-8"));				
				text[1]=reader2.readLine();

			} catch (Exception e) {
				Log.d("Async Exception", e.getLocalizedMessage());
			}
			return text;
		}

		protected void onPostExecute(String[] userResult) {
			if(userResult != null) {
				logUser(userResult);
			} 
			else {
				Toast.makeText(getApplicationContext(), "User not found, please re-enter Student ID.", Toast.LENGTH_LONG).show();
			}
		}
	}


	@Override
	protected void onStop() {
		super.onStop();  // Always call the superclass method first
	}
}
