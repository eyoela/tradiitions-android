package com.android.tradiitions.activities;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.tradiitions.R;
import com.android.tradiitions.objects.Event;
import com.android.tradiitions.util.ImageDownloader;

/* Event Detail Page
 * Author: Alex Yu
 * Date:1/31/2014
 * Displays details of an event
 * Returns to home page
 * */

/* TODO: performance enhancements!!! right now fires off ASync Task every time
 * look into object persistence? and activity life cycle management*/

public class EventDetail extends Activity {
	Event event;
	TextView textV;
	ImageView imageV;
	Drawable d = null;
	ImageDownloader iDownloader = new ImageDownloader();
	private final String GETURL = "http://techtradiitions.com:8080/";
	private ProgressBar spinner;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_event_detail);

		Intent intent = getIntent();
		event = intent.getParcelableExtra("FOREVENTDETAIL");
		textV = (TextView) findViewById(R.id.description);
		textV.setMovementMethod(new ScrollingMovementMethod());
		imageV = (ImageView) findViewById(R.id.event_image);
		spinner = (ProgressBar)findViewById(R.id.progressBar1);
		if (savedInstanceState == null) {
			new EventDetailTask().execute();
		}
	}

	protected void setEventDetails(String eventResult) {
		JSONObject eventJson;
		try {
			eventJson = new JSONObject(eventResult);
			JSONObject eventJSON = eventJson.getJSONObject("message").getJSONObject("objects");
			event.setImageURL(eventJSON.getString("event_image"));
			event.setEventURL(eventJSON.getString("event_url"));
			event.setDescription(eventJSON.getString("description"));
			event.setLocation(eventJSON.getString("location"));
			event.setEvent_score(eventJSON.getInt("event_score"));
			setViews();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	// TODO: set other information (score, location, etc.)
	protected void setViews() {
		iDownloader.download(GETURL+event.getImageURL(), imageV, spinner);
		textV.setText(event.getDescription());
		textV = (TextView) findViewById(R.id.date);
		textV.setText(event.getTime());
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.event_detail, menu);
		return true;
	}
	
	// Creating new activity for Qr Code Scanner
	public void qrscan(View view) {
		startActivity(new Intent(getApplicationContext(),QrScanActivity.class));
	}
	
	private class EventDetailTask extends AsyncTask <Void, Void, String> {
		@Override
		protected String doInBackground(Void... voids) {
			HttpClient httpClient = new DefaultHttpClient();
			HttpContext localContext = new BasicHttpContext();
			String getEventURL = GETURL+"events/"+event.getId();
			HttpGet httpGetEvent = new HttpGet(getEventURL);

			String text = new String();
			try {			
				HttpResponse response = httpClient.execute(httpGetEvent, localContext);
				HttpEntity entity = response.getEntity();
				BufferedReader reader = new BufferedReader(new InputStreamReader(entity.getContent(), "UTF-8"));				
				text=reader.readLine();
			} catch (Exception e) {
				Log.d("Async Exception", e.getLocalizedMessage());
			}
			return text;
		}

		protected void onPostExecute(String eventResult) {
			if(eventResult != null) {
				setEventDetails(eventResult);
			} 
			else {
				Toast.makeText(getApplicationContext(), "Event data not found.", Toast.LENGTH_LONG).show();
			}
		}
	}
	
	@Override
	protected void onStart() {
	    super.onStart();  // Always call the superclass method first
	    Log.d("Event onStart", event.toString());
	    // The activity is either being restarted or started for the first time
	}
	@Override
	protected void onResume() {
		super.onResume();
	    Log.d("Event onResume", event.toString());
	}
}
