package com.android.tradiitions.util;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.tradiitions.R;
import com.android.tradiitions.objects.Event;

/* Event list row adaptor view
 * Author: Alex Yu
 * Date:1/31/2014
 * Adaptor for event list page
 * */

/* TODO: research performance enhancements*/

public class EventArrayAdapter extends ArrayAdapter<Event> {
    int resource;
    Activity context;
    List<Event> elist;
    
    static class ViewHolder {
        public TextView text;
        public TextView text2;
        public ImageView image;
    }
    
    //Initialize adapter
    public EventArrayAdapter(Activity context, int resource, List<Event> items) {
        super(context, resource, items);
        this.resource=resource;
        this.context = context;
        this.elist = items;
    }
     
     
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
	    View rowView = convertView;
		if (rowView == null) {
		  LayoutInflater inflater = context.getLayoutInflater();
		  rowView = inflater.inflate(R.layout.event_list_adapter, null);
		  ViewHolder viewHolder = new ViewHolder();
		  viewHolder.text = (TextView) rowView.findViewById(R.id.eventTitle);
	      viewHolder.text2 =(TextView) rowView.findViewById(R.id.eventDate);
		  viewHolder.image = (ImageView) rowView
		      .findViewById(R.id.icon);
		  rowView.setTag(viewHolder);
	    }

        Event ev = getItem(position);

        ViewHolder holder = (ViewHolder) rowView.getTag();
         
        //Assign the appropriate data from our alert object above
        holder.text.setText(ev.getTitle());
        holder.text2.setText(ev.getTime().toString());
        holder.image.setImageResource(R.drawable.detailarrow_icon);
        return rowView;
  }
} 