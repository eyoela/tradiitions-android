package com.android.tradiitions.util;

import com.android.tradiitions.activities.LoginActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

/* Session Manager 
 * Author: Alex Yu
 * Date:1/31/2014
 * Manages shared preferences for login data of user
 * Heavily based off of http://www.androidhive.info/2012/08/android-session-management-using-shared-preferences/ 
 * by Ravi Tamada
 * */

/* TODO: use to check if user is logged in on application start*/

public class SessionManager {
	
	SharedPreferences pref;
    Editor editor;
     
    Context _context;
     
    // Sharedpref file name
    private static final String PREF_NAME = "com.tradiitions.preference";
     
    // All Shared Preferences Keys
    private static final String IS_LOGIN = "IsLoggedIn";
     
    // User name (make variable public to access from outside)
    public static final String USERNAME = "name";
    
    // Account name
    public static final String FULLNAME = "fullname";
     
    // Email address (make variable public to access from outside)
    public static final String PASSWORD = "pass";
     
    // Constructor
    public SessionManager(Context context){
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, 0);
        editor = pref.edit();
    }
             
    public void createLoginSession(String un, String pw, String fn){
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(USERNAME, un);
        editor.putString(PASSWORD, pw);
        editor.putString(FULLNAME, fn);
        editor.commit();
    }   
     
    /**
     * Check login method wil check user login status
     * If false it will redirect user to login page
     * Else won't do anything
     * */
    public void checkLogin(){
        // Check login status
        if(!this.isLoggedIn()){
            Intent i = new Intent(_context, LoginActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            _context.startActivity(i);
        }     
    }
     
    public String getUsername(){
        return pref.getString(USERNAME, null);
    }
    
    public String getPassword(){
        return pref.getString(PASSWORD, null);
    }  
    
    public String getFullname(){
        return pref.getString(FULLNAME, null);
    }  
     
    /**
     * Clear session details
     * */
    public void logoutUser(){
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();

        Intent i = new Intent(_context, LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
         
        _context.startActivity(i);
    }
     
    /**
     * Quick check for login
     * **/
    // Get Login State
    public boolean isLoggedIn(){
        return pref.getBoolean(IS_LOGIN, false);
    }
}
